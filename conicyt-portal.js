angular.module('conicytPortal', [
        'ngMaterial',
        'ngCookies'
    ])
    .provider("$conicytPortal", function () {
        var serviceURL;
        var context = 'EXPLORADOR';

        return {
            setServiceURL: function (c) {
                serviceURL = c;
            },

            setContext: function (c) {
                context = c;
            },

            $get: function ($q, $http, $window) {
                var config;

                var self = {
                    getConfig: function () {
                        var defer = $q.defer();

                        if (!_.isEmpty(config)) {
                            defer.resolve(config.CONTEXTS[context]);
                        }
                        else {

                            $http({
                                    method: 'GET',
                                    url: serviceURL + '/config'
                                }
                            ).then(
                                function (c) {
                                    config = c.data;
                                    defer.resolve(config.CONTEXTS[context]);
                                },
                                function (error) {
                                    defer.reject(error);
                                }
                            );
                        }

                        return defer.promise;
                    },
                    redirectToLogin: function () {
                        self.getConfig().then(function (config) {
                            /*ga('send', {
                             hitType: 'event',
                             eventCategory: 'Button',
                             eventAction: 'Login',
                             eventLabel: 'Iniciar Sesión'
                             });*/

                            $window.location.href = self.getLoginURL(config);
                        });
                    },
                    getLoginURL: function (config) {
                        var clientId = config.AUTH_CLIENT_ID;
                        var callback = config.AUTH_CALLBACK;

                        return config.AUTH_SOCKET + "/oauth2/authorize?scope=openid&response_type=id_token token&nonce=123&redirect_uri=" + window.encodeURIComponent(callback) + "&client_id=" + clientId;
                    },
                    redirectToLogout: function () {
                        self.getConfig().then(function (config) {
                            /*ga('send', {
                             hitType: 'event',
                             eventCategory: 'Button',
                             eventAction: 'Logout',
                             eventLabel: 'Cerrar Sesión'
                             });*/

                            var callback = config.AUTH_CALLBACK;

                            $window.location.href = config.AUTH_SOCKET + "/commonauth?commonAuthLogout=true&type=oidc2&sessionDataKey=&relyingParty=ventanilla&commonAuthCallerPath=" + window.encodeURIComponent(callback)
                        });
                    },

                    getMenu: function () {
                        var defer = $q.defer();

                        $http({
                                method: 'GET',
                                url: serviceURL + '/menu'
                            }
                        ).then(
                            function (menu) {
                                defer.resolve(menu);
                            },
                            function (error) {
                                defer.reject(error);
                            }
                        );

                        return defer.promise;
                    }
                };

                return self
            }
        }
    })
    .factory('SessionService', function ($cookies, $q, $conicytPortal) {
        return {
            getUser: function () {
                var defer = $q.defer();

                $conicytPortal.getConfig().then(function (config) {
                    var jsonString = $cookies.get(config.COOKIE);
                    if (_.isUndefined(jsonString)) {
                        defer.reject(undefined);
                    }
                    else {
                        defer.resolve(JSON.parse(jsonString));
                    }
                });

                return defer.promise;
            },

            setUser: function (user, expires) {
                var defer = $q.defer();

                $conicytPortal.getConfig().then(function (config) {
                    $cookies.put(config.COOKIE, JSON.stringify(user), {
                        domain: config.COOKIE_DOMAIN,
                        path: config.COOKIE_PATH
                    });

                    defer.resolve(user);
                });

                return defer.promise;
            },

            destroy: function () {
                var defer = $q.defer();

                $conicytPortal.getConfig().then(function (config) {
                    $cookies.put(config.COOKIE, undefined, {domain: config.COOKIE_DOMAIN, path: config.COOKIE_PATH});
                    defer.resolve(true);
                    $conicytPortal.redirectToLogout();
                });

                return defer.promise;
            }
        }
    })
    .service('User', function () {
        function User(data, accessToken) {
            this.data = data;
            this.accessToken = accessToken;
        }

        return User;
    })
    .controller('ConicytHeaderController', function ($scope, $http, $rootScope, $window, $state, $conicytPortal, SessionService) {
        var config;
        $conicytPortal.getConfig().then(function (config) {
            $scope.logo = config.PORTAL_FRONTEND + 'assets/img/logo-plano.gif';
            fillUserInfo();
            $scope.redirectToLogin = $conicytPortal.redirectToLogin;
            $scope.redirectToLogout = function () {
                SessionService.destroy();
            }
        });

        $scope.logged = null;
        $scope.menuItems = [];

        function fillUserInfo() {
            var user = SessionService.getUser().then(function (user) {
                if (_.isUndefined(user)) {
                    $scope.logged = false;
                }
                else {
                    $scope.logged = true;

                    if (user.data.fuente == 'ads.conicyt.cl') {
                        $scope.userName = user.data.nombre;
                    }
                    else {
                        $scope.userName = user.data.primerNombre + ' ' + user.data.primerApellido;
                    }

                    if (!_.isEmpty(user.data) && !_.isEmpty(user.data.correo)) {

                        $conicytPortal.getMenu().then(
                            function (items) {
                                $scope.menuItems = _.map(_.filter(items.data,
                                    function (item) {
                                        return _.includes(item.allowed, user.data.correo);
                                    }),
                                    function (item) {
                                        item.inUrl = window.encodeURIComponent(item.inUrl);
                                        return item;
                                    });
                            },
                            function (response) {
                                console.log(response);
                            }
                        );

                    }

                    $scope.goToMenuItem = function (item) {
                        var url = item.inUrl;

                        if (item.inUrl.charAt(item.inUrl.length - 1) == "=") {
                            url = item.inUrl + user.accessToken
                        }

                        $window.location.href = window.decodeURIComponent(url);
                    };
                }
            });
        }

        $scope.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        }
    })
    .directive('conicytHeader', function () {
        return {
            restrict: 'E',
            scope: {
                //config: '='
            },
            template: '<md-content ng-controller="ConicytHeaderController"> ' +
            '<md-toolbar style="background: #f5f5f5"> ' +
            '<div class="md-toolbar-tools"> ' +
            '<div flex="20" layout="row" layout-align="center center"> ' +
            '<div flex="50" style="background-image: url({{logo}});background-size: contain">&nbsp;</div> ' +
            '<div flex layout-align="center top"> ' +
            '<span style="color: #2f6dae">&nbsp;&nbsp;</span> ' +
            '</div> ' +
            '</div> ' +
            '<div flex></div> ' +
            '<div layout="row" ng-if="_.size(menuItems) > 1"> ' +
            '<div ng-repeat="i in menuItems"> ' +
            '<a href="#" ng-click="goToMenuItem(i)"> ' +
            '<span style="color:#2f6dae;font-size: medium">{{i.name}}</span> ' +
            '</a> ' +
            '</div> ' +
            '</div> ' +
            '<!--div> ' +
            '<p><span style="color:#2f6dae;font-size: medium">{{::userName}}</span></p> ' +
            '</div--> ' +
            '<div> ' +
            '<md-menu ng-if="logged"> ' +
            '<md-button aria-label="Open phone interactions menu" class="md-icon-button"' +
            'ng-click="openMenu($mdOpenMenu, $event)"> ' +
            '<i class="material-icons" style="color: black;">menu</i> ' +
            '</md-button> ' +
            '<md-menu-content width="4"> ' +
            '<md-menu-item ng-repeat="i in menuItems"> ' +
            '<md-button ng-click="goToMenuItem(i)">' +
            '{{i.name}} ' +
            '</md-button> ' +
            '</md-menu-item> ' +
            '<md-menu-divider></md-menu-divider> ' +
            '<md-menu-item> ' +
            '<md-button ng-click="redirectToLogout()">' +
            'Cerrar Sesión ' +
            '</md-button> ' +
            '</md-menu-item> ' +
            '</md-menu-content> ' +
            '</md-menu> ' +
            '<md-button style="color: #2f6dae" aria-label="Login" ng-click="redirectToLogin()" ng-if="!logged">' +
            'Iniciar Sesión ' +
            '<!--span class="glyphicon glyphicon-log-in"></span--> ' +
            '<!--md-icon md-svg-icon="img/icons/more_vert.svg"></md-icon--> ' +
            '</md-button> ' +
            '<!--md-button class="md-icon-button" aria-label="Settings" ng-disabled="true"> ' +
            '<i class="material-icons">menu</i> ' +
            '</md-button--> ' +
            '</div> ' +
            '</div> ' +
            '</md-toolbar> ' +
            '</md-content>'
        };
    });
